#!/usr/bin/env hy
(setv foobar (+ 2 2))
(setv [tim eric] ["jim" "derrick"])
(setv  alpha "a"  beta "b")

(sorted "abcBC"
  :key (fn [x] (.lower x)))

(defn test [a b [c "x"] #* d]
  [a b c d])

(lfor
  x (range 3)
  y (range 3)
  :if (= (+ x y) 3)
  (* x y))

(defmacro do-while [test #* body]
  `(do
    ~@body
    (while ~test
      ~@body)))

(setv x 0)
(do-while x
  (print "Printed once."))

